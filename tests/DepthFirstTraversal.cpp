// Copyright 2018, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Tests for cxxgraph/DepthFirstTraversal.h
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

// Internal Includes
#include "Configs.h"
#include "Matcher.h"

#include "cxxgraph/AdjacencyListGraph.h"
#include "cxxgraph/DepthFirstTraversal.h"
#include "cxxgraph/Stream.h"
#include "cxxgraph/TraversalTrace.h"

// Library Includes
#include <catch2/catch.hpp>

// Standard Includes
#include <sstream>

using namespace cxxgraph::traversal;

namespace {
/*

Graph:

a  ->  b

|      |
v      v

c  ->  d

*/
template <typename GraphConfig>
struct DFTFixture
{

	using Graph = cxxgraph::adjacency_list::Graph<GraphConfig>;
	using DFT = DepthFirstTraversal<Graph>;
	using vertex_const_descriptor_t = cxxgraph::graph_vertex_const_descriptor_t<Graph>;
	using edge_const_descriptor_t = cxxgraph::graph_edge_const_descriptor_t<Graph>;

	DFTFixture()
	    : g(), a(g.addVertex("a")), b(g.addVertex("b")), c(g.addVertex("c")), d(g.addVertex("d")),
	      ab(g.addEdge(a, b, "ab")), ac(g.addEdge(a, c, "ac")), cd(g.addEdge(c, d, "cd")), bd(g.addEdge(b, d, "bd"))
	{}

	Graph g;
	vertex_const_descriptor_t a;
	vertex_const_descriptor_t b;
	vertex_const_descriptor_t c;
	vertex_const_descriptor_t d;

	edge_const_descriptor_t ab;
	edge_const_descriptor_t ac;
	edge_const_descriptor_t cd;
	edge_const_descriptor_t bd;

	void verifyDFTfromA(DFT const& dft) const
	{
		// a is its own predecessor
		REQUIRE(dft.predecessors[a] == a);
		// nothing else is its own predecessor
		REQUIRE(dft.predecessors[b] != b);
		REQUIRE(dft.predecessors[c] != c);
		REQUIRE(dft.predecessors[d] != d);
		// no descriptors should be default constructed.
		REQUIRE(dft.predecessors[b] != vertex_const_descriptor_t{});
		REQUIRE(dft.predecessors[c] != vertex_const_descriptor_t{});
		REQUIRE(dft.predecessors[d] != vertex_const_descriptor_t{});
		for (auto p : dft.predecessors.getRange()) {
			if (p.first == a) {
				// a is its own predecessor
				REQUIRE(p.first == p.second);
			} else {
				// nothing else is its own predecessor
				REQUIRE(p.first != p.second);
			}
			// no descriptors should be default constructed.
			REQUIRE_FALSE(p.first == vertex_const_descriptor_t{});
			REQUIRE_FALSE(p.second == vertex_const_descriptor_t{});
		}

		REQUIRE(dft.predecessors[b] == a);
		REQUIRE(dft.predecessors[c] == a);
		REQUIRE_THAT(dft.predecessors[d], IsInSet({b, c}));
	}

	void verifyDFTfromAUndir(DFT const& dft) const
	{
		// a is its own predecessor
		REQUIRE(dft.predecessors[a] == a);
		REQUIRE_THAT(dft.predecessors[b], IsInSet({a, d}));
		REQUIRE_THAT(dft.predecessors[c], IsInSet({a, d}));
		REQUIRE_THAT(dft.predecessors[d], IsInSet({b, c}));
	}

	void verifyDFTfromBDirected(DFT const& dft) const
	{
		CHECK(dft.predecessors[a] == vertex_const_descriptor_t{});
		CHECK(dft.predecessors[b] == b);
		CHECK(dft.predecessors[c] == vertex_const_descriptor_t{});
		CHECK(dft.predecessors[d] == b);
	}

	void verifyDFTfromBUndirected(DFT const& dft) const
	{
		REQUIRE_THAT(dft.predecessors[a], IsInSet({b, c}));
		REQUIRE(dft.predecessors[b] == b);
		REQUIRE_THAT(dft.predecessors[c], IsInSet({a, d}));
		REQUIRE_THAT(dft.predecessors[d], IsInSet({b, c}));
	}

	void verifyDFTfromDReverseDirected(DFT const& dft) const
	{
		REQUIRE_THAT(dft.predecessors[a], IsInSet({b, c}));
		REQUIRE(dft.predecessors[b] == d);
		REQUIRE(dft.predecessors[c] == d);
		REQUIRE(dft.predecessors[d] == d);
	}

	template <typename... Args, typename F>
	void doTraversal(F&& bftChecker, Args&&... args) const
	{
		INFO("a = " << a);
		INFO("b = " << b);
		INFO("c = " << c);
		INFO("d = " << d);
		INFO("ab = " << ab);
		INFO("ac = " << ac);
		INFO("cd = " << cd);
		INFO("bd = " << bd);
		{
			DFT dft{g};
			auto t = trace(dft);
			traverse(g, t, std::forward<Args>(args)...);
			INFO(t.os.str());
			bftChecker(dft);
		}
		{
			DFT dft{g};
			traverse(g, dft, std::forward<Args>(args)...);
			bftChecker(dft);
		}
	}
	void testDirected() const
	{
		WHEN("Traversing as a directed graph from a")
		{
			doTraversal([&](auto& dft) { verifyDFTfromA(dft); }, a);
		}
		WHEN("Traversing as a directed graph from b")
		{
			doTraversal([&](auto& dft) { verifyDFTfromBDirected(dft); }, b);
		}
	}

	void testBidi() const
	{
		WHEN("Traversing as a directed graph from a")
		{
			doTraversal([&](auto& dft) { verifyDFTfromA(dft); }, a, cxxgraph::DirectedBehavior::Directed);
		}
		WHEN("Traversing as a directed graph from b")
		{
			doTraversal([&](auto& dft) { verifyDFTfromBDirected(dft); }, b,
			            cxxgraph::DirectedBehavior::Directed);
		}

		WHEN("Traversing bidirectionally from a")
		{
			doTraversal([&](auto& dft) { verifyDFTfromAUndir(dft); }, a,
			            cxxgraph::DirectedBehavior::Bidirectional);
		}
		WHEN("Traversing bidirectionally from b")
		{
			doTraversal([&](auto& dft) { verifyDFTfromBUndirected(dft); }, b,
			            cxxgraph::DirectedBehavior::Bidirectional);
		}
		WHEN("Traversing directed from a, stopping when b is found")
		{
			auto checker = [&](auto& dft) {
				REQUIRE(dft.predecessors[a] == a);
				REQUIRE(dft.predecessors[b] == a);
				REQUIRE_THAT(dft.predecessors[c], IsInSet({a, vertex_const_descriptor_t{}}));
				REQUIRE_THAT(dft.predecessors[d], IsInSet({c, vertex_const_descriptor_t{}}));
			};
			doTraversal(checker, a, cxxgraph::DirectedBehavior::Directed,
			            [&](auto& desc) { return desc == b; });
		}
		WHEN("Traversing bidirectionally from b, stopping when d is found")
		{
			auto checker = [&](auto& dft) {
				REQUIRE_THAT(dft.predecessors[a], IsInSet({b, vertex_const_descriptor_t{}}));
				REQUIRE(dft.predecessors[b] == b);
				REQUIRE_THAT(dft.predecessors[c], IsInSet({a, vertex_const_descriptor_t{}}));
				REQUIRE_THAT(dft.predecessors[d], IsInSet({b, c}));
			};
			doTraversal(checker, b, cxxgraph::DirectedBehavior::Bidirectional,
			            [&](auto& desc) { return desc == d; });
		}
		WHEN("Traversing reverse-directed from d")
		{
			doTraversal([&](auto& dft) { verifyDFTfromDReverseDirected(dft); }, d,
			            cxxgraph::DirectedBehavior::ReverseDirected);
		}
	}
	void testUndir() const
	{
		WHEN("Traversing from a")
		{
			doTraversal([&](auto& dft) { verifyDFTfromAUndir(dft); }, a);
		}
		WHEN("Traversing from b")
		{
			doTraversal([&](auto& dft) { verifyDFTfromBUndirected(dft); }, b);
		}
		WHEN("Traversing from d")
		{
			auto check = [&](auto& dft) {
				// d is its own predecessor
				REQUIRE(dft.predecessors[d] == d);
				REQUIRE_THAT(dft.predecessors[a], IsInSet({b, c}));
				REQUIRE_THAT(dft.predecessors[b], IsInSet({a, d}));
				REQUIRE_THAT(dft.predecessors[c], IsInSet({a, d}));
			};
			doTraversal(check, d);
		}
	}
};
} // namespace

TEST_CASE_METHOD(DFTFixture<my_config::VecVecStringStringConfig>, "DFT-VecVecStringString") { testDirected(); }

TEST_CASE_METHOD(DFTFixture<my_config::BidiVecVecStringStringConfig>, "DFT-BidiVecVecStringString") { testBidi(); }

#ifndef GRAPH_LIGHTER_TESTS
TEST_CASE_METHOD(DFTFixture<my_config::UndirVecVecStringStringConfig>, "DFT-UndirVecVecStringString") { testUndir(); }

TEST_CASE_METHOD(DFTFixture<my_config::ListVecStringStringConfig>, "DFT-ListVecStringString") { testDirected(); }

TEST_CASE_METHOD(DFTFixture<my_config::VecListStringStringConfig>, "DFT-VecListStringString") { testDirected(); }

TEST_CASE_METHOD(DFTFixture<my_config::BidiVecListStringStringConfig>, "DFT-BidiVecListStringString") { testBidi(); }
#endif // !GRAPH_LIGHTER_TESTS

TEST_CASE_METHOD(DFTFixture<my_config::UndirVecListStringStringConfig>, "DFT-UndirVecListStringString") { testUndir(); }

TEST_CASE_METHOD(DFTFixture<my_config::SmListVecStringStringConfig>, "DFT-SmListVecStringString") { testDirected(); }
