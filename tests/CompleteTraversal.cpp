// Copyright 2018, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Tests for cxxgraph/CompleteTraversal.h wrapping BreadthFirstTraversal
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

// Internal Includes
#include "Configs.h"
#include "Matcher.h"

#include "cxxgraph/AdjacencyListGraph.h"
#include "cxxgraph/BreadthFirstTraversal.h"
#include "cxxgraph/CompleteTraversal.h"
#include "cxxgraph/Stream.h"
#include "cxxgraph/TraversalTrace.h"

// Library Includes
#include <catch2/catch.hpp>

// Standard Includes
#include <sstream>

using namespace cxxgraph::traversal;

namespace {
/*

Graph:

a  ->  b

|      |
v      v

c  ->  d

*/
template <typename GraphConfig>
struct TraversalFixture
{

	using Graph = cxxgraph::adjacency_list::Graph<GraphConfig>;
	using BFT = BreadthFirstTraversal<Graph>;
	using CompleteBFT = CompleteTraversalStrategy<BFT>;
	using vertex_const_descriptor_t = cxxgraph::graph_vertex_const_descriptor_t<Graph>;
	using edge_const_descriptor_t = cxxgraph::graph_edge_const_descriptor_t<Graph>;

	TraversalFixture()
	    : g(), a(g.addVertex("a")), b(g.addVertex("b")), c(g.addVertex("c")), d(g.addVertex("d")),
	      ab(g.addEdge(a, b, "ab")), ac(g.addEdge(a, c, "ac")), cd(g.addEdge(c, d, "cd")), bd(g.addEdge(b, d, "bd"))
	{}

	Graph g;
	vertex_const_descriptor_t a;
	vertex_const_descriptor_t b;
	vertex_const_descriptor_t c;
	vertex_const_descriptor_t d;

	edge_const_descriptor_t ab;
	edge_const_descriptor_t ac;
	edge_const_descriptor_t cd;
	edge_const_descriptor_t bd;

	void verifyBFTfromA(BFT const& bft) const
	{
		REQUIRE(bft.predecessors[a] == a);
		REQUIRE(bft.predecessors[b] == a);
		REQUIRE(bft.predecessors[c] == a);
		REQUIRE_THAT(bft.predecessors[d], IsInSet({b, c}));
	}

	void verifyBFTfromBDirected(BFT const& bft) const
	{
		CHECK(bft.predecessors[a] == a); // extra root from CompleteTraversalStrategy
		CHECK(bft.predecessors[b] == b);
		CHECK(bft.predecessors[c] == a);
		CHECK(bft.predecessors[d] == b);
	}
	void verifyBFTfromBUndirected(BFT const& bft) const
	{
		REQUIRE(bft.predecessors[a] == b);
		REQUIRE(bft.predecessors[b] == b);
		REQUIRE_THAT(bft.predecessors[c], IsInSet({a, d}));
		REQUIRE(bft.predecessors[d] == b);
	}
	void verifyBFTfromDReverseDirected(BFT const& bft) const
	{
		REQUIRE_THAT(bft.predecessors[a], IsInSet({b, c}));
		REQUIRE(bft.predecessors[b] == d);
		REQUIRE(bft.predecessors[c] == d);
		REQUIRE(bft.predecessors[d] == d);
	}

	template <typename... Args, typename F>
	void doTraversal(F&& bftChecker, Args&&... args) const
	{
		INFO("a = " << a);
		INFO("b = " << b);
		INFO("c = " << c);
		INFO("d = " << d);
		INFO("ab = " << ab);
		INFO("ac = " << ac);
		INFO("cd = " << cd);
		INFO("bd = " << bd);
		{
			BFT bft{g};
			auto t = trace(bft);
			auto complete = completeTraversal(g, t);
			traverse(g, complete, std::forward<Args>(args)...);
			INFO(t.os.str());
			bftChecker(bft);
		}
		{
			BFT bft{g};
			auto complete = completeTraversal(g, bft);
			traverse(g, complete, std::forward<Args>(args)...);
			bftChecker(bft);
		}
	}
	void testDirected() const
	{
		WHEN("Traversing as a directed graph from a")
		{
			doTraversal([&](auto& bft) { verifyBFTfromA(bft); }, a);
		}
		WHEN("Traversing as a directed graph from b")
		{
			doTraversal([&](auto& bft) { verifyBFTfromBDirected(bft); }, b);
		}
	}

	void testBidi() const
	{
		WHEN("Traversing as a directed graph from a")
		{
			doTraversal([&](auto& bft) { verifyBFTfromA(bft); }, a, cxxgraph::DirectedBehavior::Directed);
		}
		WHEN("Traversing as a directed graph from b")
		{
			doTraversal([&](auto& bft) { verifyBFTfromBDirected(bft); }, b,
			            cxxgraph::DirectedBehavior::Directed);
		}

		WHEN("Traversing bidirectionally from a")
		{
			doTraversal([&](auto& bft) { verifyBFTfromA(bft); }, a,
			            cxxgraph::DirectedBehavior::Bidirectional);
		}
		WHEN("Traversing bidirectionally from b")
		{
			doTraversal([&](auto& bft) { verifyBFTfromBUndirected(bft); }, b,
			            cxxgraph::DirectedBehavior::Bidirectional);
		}
		WHEN("Traversing bidirectionally from b, stopping when d is found")
		{
			auto checker = [&](auto& bft) {
				REQUIRE_THAT(bft.predecessors[a], IsInSet({b, vertex_const_descriptor_t{}}));
				REQUIRE(bft.predecessors[b] == b);
				REQUIRE(bft.predecessors[c] == vertex_const_descriptor_t{});
				REQUIRE(bft.predecessors[d] == b);
			};
			doTraversal(checker, b, cxxgraph::DirectedBehavior::Directed,
			            [&](auto& desc) { return desc == d; });
		}
		WHEN("Traversing reverse-directed from d")
		{
			doTraversal([&](auto& bft) { verifyBFTfromDReverseDirected(bft); }, d,
			            cxxgraph::DirectedBehavior::ReverseDirected);
		}
	}
	void testUndir() const
	{
		WHEN("Traversing from a")
		{
			doTraversal([&](auto& bft) { verifyBFTfromA(bft); }, a);
		}
		WHEN("Traversing from b")
		{
			doTraversal([&](auto& bft) { verifyBFTfromBUndirected(bft); }, b);
		}
		WHEN("Traversing from d")
		{
			doTraversal([&](auto& bft) { verifyBFTfromDReverseDirected(bft); }, d);
		}
	}
};
} // namespace

TEST_CASE_METHOD(TraversalFixture<my_config::VecVecStringStringConfig>, "CompleteBFT-VecVecStringString")
{
	testDirected();
}

TEST_CASE_METHOD(TraversalFixture<my_config::BidiVecVecStringStringConfig>, "CompleteBFT-BidiVecVecStringString")
{
	testBidi();
}

#ifndef GRAPH_LIGHTER_TESTS
TEST_CASE_METHOD(TraversalFixture<my_config::UndirVecVecStringStringConfig>, "CompleteBFT-UndirVecVecStringString")
{
	testUndir();
}

TEST_CASE_METHOD(TraversalFixture<my_config::ListVecStringStringConfig>, "CompleteBFT-ListVecStringString")
{
	testDirected();
}

TEST_CASE_METHOD(TraversalFixture<my_config::VecListStringStringConfig>, "CompleteBFT-VecListStringString")
{
	testDirected();
}

TEST_CASE_METHOD(TraversalFixture<my_config::BidiVecListStringStringConfig>, "CompleteBFT-BidiVecListStringString")
{
	testBidi();
}
#endif // !GRAPH_LIGHTER_TESTS

TEST_CASE_METHOD(TraversalFixture<my_config::UndirVecListStringStringConfig>, "CompleteBFT-UndirVecListStringString")
{
	testUndir();
}

TEST_CASE_METHOD(TraversalFixture<my_config::SmListVecStringStringConfig>, "CompleteBFT-SmListVecStringString")
{
	testDirected();
}
