// Copyright 2018, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Tests for cxxgraph/Traversal.h
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

// Internal Includes
#include "Configs.h"
#include "cxxgraph/AdjacencyListGraph.h"
#include "cxxgraph/BreadthFirstTraversal.h"
#include "cxxgraph/Traversal.h"

// Library Includes
#include <catch2/catch.hpp>

// Standard Includes
// - none

namespace cxxgraph::traversal::detail {

template <typename T>
struct EarlyStoppingPredicateWrapper
{
	std::reference_wrapper<T> f;
	explicit EarlyStoppingPredicateWrapper(std::reference_wrapper<T> func) : f(func) {}
};
template <typename T>
struct StrategyWrapper
{
	std::reference_wrapper<T> strategy;
	explicit StrategyWrapper(std::reference_wrapper<T> s) : strategy(s) {}
};
template <typename Derived>
struct TraversalOptionsBase
{
	//! CRTP accessor for constant derived type.
	constexpr Derived const& derived() const noexcept { return static_cast<Derived const&>(*this); }

	//! CRTP accessor for derived type.
	constexpr Derived& derived() noexcept { return static_cast<Derived&>(*this); }

private:
	// Avoids mistaken usage by derived classes:
	// see https://www.fluentcpp.com/2017/05/12/curiously-recurring-template-pattern/
	constexpr TraversalOptionsBase() = default;
	friend Derived;
};

template <typename T>
struct GetTupleTypeList;
template <>
struct GetTupleTypeList<std::tuple<>>
{
	using type = meta::list<>;
};
template <typename... Ts>
struct GetTupleTypeList<std::tuple<Ts...>>
{
	using type = meta::list<Ts...>;
};
template <typename TupleType>
using tuple_type_list_t = meta::_t<GetTupleTypeList<traits::stripped_t<TupleType>>>;

template <template <typename> class Template>
struct IsConstRefInstantiation
{
	template <typename T>
	using invoke = meta::is<traits::stripped_t<traits::stripped_t<T>>, Template>;
};
template <template <typename> class Template, typename TupleType>
using template_from_tuple_tail_t = meta::find_if<tuple_type_list_t<TupleType>, IsConstRefInstantiation<Template>>;

template <template <typename> class Template, typename TupleType>
using is_template_in_tuple_t = meta::not_<meta::empty<template_from_tuple_tail_t<Template, TupleType>>>;
#if 0
template <template <typename> class Template, typename TupleType>
struct GetTemplateFromTupleImpl
    : std::conditional_t<is_template_in_tuple_v<Template, TupleType>,                        //
                         meta::lazy::front<template_from_tuple_tail_t<Template, TupleType>>, //
                         meta::id<void>,                                                     //
                         >
{
};
#endif

template <template <typename> class Template, typename TupleType>
constexpr decltype(auto) getTemplateInstantiation(TupleType&& t)
{
	using tail = template_from_tuple_tail_t<Template, TupleType>;
	if constexpr (!meta::empty<tail>::value) {
		return std::get<meta::front<tail>>(t);
	}
}
#if 0

template <template <typename> class Template, typename TupleType>

using template_instantiation_in_tuple_t = meta::_t<GetTemplateFromTupleImpl<Template, TupleType>>;
using template_instantiation_in_tuple_t =
    meta::_t<std::conditional_t<is_template_in_tuple_t<Template, TupleType>::value,                 //
                                meta::lazy::front<template_from_tuple_tail_t<Template, TupleType>>, //
                                meta::id<void>                                                      //
                                >>;
#endif

template <typename Graph, typename TypeList = meta::list<>, typename TupleType = std::tuple<>>
struct TraversalOptions : TraversalOptionsBase<TraversalOptions<Graph, TypeList, TupleType>>
{
	using graph_t = Graph;
	using type_list_t = TypeList;
	using tuple_t = TupleType;
	constexpr TraversalOptions(std::reference_wrapper<const Graph> const& g, TupleType&& d) noexcept
	    : graph(g), data(std::move(d))
	{}
	constexpr TraversalOptions(GraphBase<Graph> const& g) noexcept : graph(std::cref(g.derived())) {}

	std::reference_wrapper<const Graph> graph;
	TupleType data;

	/*! Specificy a directedness for traversal.
	 *
	 * Only valid if graph is bidirectional-directed - and required in that case as well.
	 */
	constexpr auto directedness(DirectedBehavior const& directedness) const noexcept
	{

		static_assert(graph_is_bidirectional_directed_v<Graph>,
		              "Must supply a directedness if graph is bidirectional-directed");

		return append(directedness);
	}
	template <typename F>
	constexpr auto earlyStoppingPredicate(F&& f) const noexcept
	{
		return append(std::ref(std::forward<F>(f)));
	}

	template <template <typename> class Template>
	constexpr auto&& get() const noexcept
	{
		if constexpr (is_template_in_tuple_t<Template, tuple_t>::value) {
			return getTemplateInstantiation<Template>(data);
		}
	}

private:
	template <typename T>
	constexpr auto append() const noexcept
	{
		return TraversalOptions<Graph, meta::push_back<TypeList, T>, TupleType>{graph, std::move(data)};
	}
	template <typename... T>
	constexpr auto cat(T&&... v) const noexcept
	{
		return std::tuple_cat(data, std::make_tuple(std::forward<T>(v)...));
	}
	template <typename T>
	using with_tuple_appended_t =
	    meta::apply<meta::quote<std::tuple>, meta::push_back<tuple_type_list_t<tuple_t>, T const&>>;
	template <typename T>
	constexpr auto append(T&& v) const noexcept
	{
		return TraversalOptions<Graph, TypeList, with_tuple_appended_t<T>>{graph, cat(std::forward<T>(v))};
	}
};

template <typename T>
constexpr DirectedBehavior getDirectedness(TraversalOptionsBase<T> const& opt)
{
	using tuple_t = typename T::tuple_t;
	constexpr bool is_directedness_specified = meta::in<tuple_type_list_t<tuple_t>, DirectedBehavior const&>::value;
	if constexpr (graph_is_bidirectional_directed_v<typename T::graph_t>) {
		static_assert(is_directedness_specified,
		              "You must specify a directedness for traversing a bidirectional-directed graph.");
	}

	if constexpr (is_directedness_specified) {
		return std::get<DirectedBehavior const&>(opt.derived().data);
	} else {
		return DirectedBehavior::Directed;
	}
}
template <typename T>
constexpr decltype(auto) getEarlyStoppingPredicate(TraversalOptionsBase<T> const& opt)
{
	using tuple_t = typename T::tuple_t;
	if constexpr (is_template_in_tuple_t<EarlyStoppingPredicateWrapper, tuple_t>::value) {
		return opt.derived().template get<EarlyStoppingPredicateWrapper>().f;
	} else {
		return AlwaysFalse{};
	}
}
template <typename T>
constexpr auto&& getGraph(TraversalOptionsBase<T> const& opt)
{
	return opt.derived().graph.get();
}
template <typename T>
constexpr auto&& getStrategy(TraversalOptionsBase<T> const& opt)
{
	return opt.derived().template get<StrategyWrapper>().strategy.get();
}
} // namespace cxxgraph::traversal::detail
TEST_CASE("TraversalOptions")
{
	using namespace cxxgraph::traversal;
	using namespace cxxgraph::traversal::detail;
	using cxxgraph::DirectedBehavior;
	using Graph = cxxgraph::adjacency_list::Graph<my_config::VecVecStringStringConfig>;
	Graph g;
	using BidiGraph = cxxgraph::adjacency_list::Graph<my_config::BidiVecVecStringStringConfig>;
	BidiGraph bidiG;
	REQUIRE_NOTHROW(TraversalOptions{g});
	REQUIRE_NOTHROW(TraversalOptions{bidiG});
	REQUIRE_NOTHROW(TraversalOptions{bidiG}.directedness(DirectedBehavior::Directed));

	REQUIRE(getDirectedness(TraversalOptions{g}) == DirectedBehavior::Directed);
	REQUIRE(getDirectedness(TraversalOptions{bidiG}.directedness(DirectedBehavior::Directed)) ==
	        DirectedBehavior::Directed);

	REQUIRE(getEarlyStoppingPredicate(TraversalOptions{g})() == false);
	REQUIRE(getEarlyStoppingPredicate(TraversalOptions{bidiG})() == false);

	REQUIRE(&getGraph(TraversalOptions{g}) == &g);
	REQUIRE(&getGraph(TraversalOptions{bidiG}) == &bidiG);
#if 0
	using BFT = BreadthFirstTraversal<Graph>;
	using BidiBFT = BreadthFirstTraversal<BidiGraph>;
#endif
}
