// Copyright 2018, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Tests for cxxgraph/DijkstraShortestPathTraversal.h
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

// Internal Includes
#include "Configs.h"
#include "Matcher.h"

#include "cxxgraph/AdjacencyListGraph.h"
#include "cxxgraph/DijkstraShortestPathTraversal.h"
#include "cxxgraph/Stream.h"
#include "cxxgraph/TraversalTrace.h"

// Library Includes
#include <catch2/catch.hpp>

// Standard Includes
// - none

using namespace cxxgraph::traversal;
using traversal_t = DijkstraShortestPathTraversal<cxxgraph::adjacency_list::Graph<my_config::VecVecStringStringConfig>>;

#ifdef GRAPH_TRAV_HAVE_OPTIONAL
namespace std {
template <typename T>
static inline ostream& operator<<(ostream& os, optional<T> const& v)
{
	if (v) {
		os << *v;
	} else {
		os << "[empty optional<>]";
	}
	return os;
}
} // namespace std

#define GRAPH_CHECK_COST_OR_EMPTY(TRAV, V, COST)                                                                       \
	REQUIRE_THAT((TRAV).getMinCost(V), IsInSet({optional<uint_cost_t>{}, optional<uint_cost_t>{(COST)}}))

#define GRAPH_CHECK_COST(TRAV, V, COST)                                                                                \
	REQUIRE((TRAV).getMinCost(V) == optional<uint_cost_t>{(COST)});                                                \
	REQUIRE((TRAV).costs[(V)] == (COST))
#define GRAPH_CHECK_PRECEDING_EDGE(TRAV, V, EDGE)                                                                      \
	REQUIRE((TRAV).getPrecedingEdge(V) == optional<edge_const_descriptor_t>{(EDGE)})
#define GRAPH_CHECK_EMPTY_PRECEDING_EDGE(TRAV, V)                                                                      \
	REQUIRE((TRAV).getPrecedingEdge(V) == optional<edge_const_descriptor_t>{})
#else
#define GRAPH_CHECK_COST_OR_EMPTY(TRAV, V, COST)
#define GRAPH_CHECK_COST(TRAV, V, COST) REQUIRE((TRAV).costs[(V)] == (COST))
#define GRAPH_CHECK_PRECEDING_EDGE(TRAV, V, EDGE)
#define GRAPH_CHECK_EMPTY_PRECEDING_EDGE(TRAV, V)

#endif // GRAPH_TRAV_HAVE_OPTIONAL

namespace {
/*

Graph with edge costs:
      1
   a  ->  b

 2 |      | 3
   v      v

   c  ->  d
       4

*/
template <typename GraphConfig>
struct DijkstraFixture
{

	using Graph = cxxgraph::adjacency_list::Graph<GraphConfig>;
	using Dijkstra = DijkstraShortestPathTraversal<Graph>;
	using vertex_const_descriptor_t = cxxgraph::graph_vertex_const_descriptor_t<Graph>;
	using edge_const_descriptor_t = cxxgraph::graph_edge_const_descriptor_t<Graph>;

	DijkstraFixture()
	    : g(), a(g.addVertex("a")), b(g.addVertex("b")), c(g.addVertex("c")), d(g.addVertex("d")),
	      ab(g.addEdge(a, b, "ab")), ac(g.addEdge(a, c, "ac")), cd(g.addEdge(c, d, "cd")), bd(g.addEdge(b, d, "bd"))
	{}

	uint_cost_t getEdgeCost(edge_const_descriptor_t e) const
	{
		if (e == ab) {
			return 1;
		}
		if (e == ac) {
			return 2;
		}
		if (e == bd) {
			return 3;
		}
		if (e == cd) {
			return 4;
		}
		FAIL("Should not get here!");
		return infinite_uint_cost;
	}
	Graph g;
	vertex_const_descriptor_t a;
	vertex_const_descriptor_t b;
	vertex_const_descriptor_t c;
	vertex_const_descriptor_t d;

	edge_const_descriptor_t ab;
	edge_const_descriptor_t ac;
	edge_const_descriptor_t cd;
	edge_const_descriptor_t bd;

	void verifyDSPfromA(Dijkstra const& dsp) const
	{
		REQUIRE(dsp.predecessors[a] == a);
		GRAPH_CHECK_COST(dsp, a, 0);
		GRAPH_CHECK_PRECEDING_EDGE(dsp, a, edge_const_descriptor_t{});

		REQUIRE(dsp.predecessors[b] == a);
		GRAPH_CHECK_COST(dsp, b, 1);
		GRAPH_CHECK_PRECEDING_EDGE(dsp, b, ab);

		REQUIRE(dsp.predecessors[c] == a);
		GRAPH_CHECK_COST(dsp, c, 2);
		GRAPH_CHECK_PRECEDING_EDGE(dsp, c, ac);

		REQUIRE(dsp.predecessors[d] == b);
		GRAPH_CHECK_COST(dsp, d, 4);
		GRAPH_CHECK_PRECEDING_EDGE(dsp, d, bd);
	}

	void verifyDSPfromBDirected(Dijkstra const& dsp) const
	{
		CHECK(dsp.predecessors[a] == vertex_const_descriptor_t{});
		CHECK(dsp.costs[a] == infinite_uint_cost);
		CHECK_FALSE(dsp.visited[a]);
		GRAPH_CHECK_EMPTY_PRECEDING_EDGE(dsp, a);

		CHECK(dsp.predecessors[b] == b);
		GRAPH_CHECK_COST(dsp, b, 0);
		GRAPH_CHECK_PRECEDING_EDGE(dsp, b, edge_const_descriptor_t{});

		CHECK(dsp.predecessors[c] == vertex_const_descriptor_t{});
		CHECK(dsp.costs[c] == infinite_uint_cost);
		CHECK_FALSE(dsp.visited[c]);
		GRAPH_CHECK_EMPTY_PRECEDING_EDGE(dsp, c);

		CHECK(dsp.predecessors[d] == b);
		GRAPH_CHECK_COST(dsp, d, 3);
		GRAPH_CHECK_PRECEDING_EDGE(dsp, d, bd);
	}

	void verifyDSPfromBUndirected(Dijkstra const& dsp) const
	{
		REQUIRE(dsp.predecessors[a] == b);
		GRAPH_CHECK_COST(dsp, a, 1);
		GRAPH_CHECK_PRECEDING_EDGE(dsp, a, ab);

		REQUIRE(dsp.predecessors[b] == b);
		GRAPH_CHECK_COST(dsp, b, 0);
		GRAPH_CHECK_PRECEDING_EDGE(dsp, b, edge_const_descriptor_t{});

		REQUIRE(dsp.predecessors[c] == a);
		GRAPH_CHECK_COST(dsp, c, 3);
		GRAPH_CHECK_PRECEDING_EDGE(dsp, c, ac);

		REQUIRE(dsp.predecessors[d] == b);
		GRAPH_CHECK_COST(dsp, d, 3);
		GRAPH_CHECK_PRECEDING_EDGE(dsp, d, bd);
	}

	void verifyDSPfromDReverseDirected(Dijkstra const& dsp) const
	{
		REQUIRE(dsp.predecessors[a] == b);
		GRAPH_CHECK_COST(dsp, a, 4);
		GRAPH_CHECK_PRECEDING_EDGE(dsp, a, ab);

		REQUIRE(dsp.predecessors[b] == d);
		GRAPH_CHECK_COST(dsp, b, 3);
		GRAPH_CHECK_PRECEDING_EDGE(dsp, b, bd);

		REQUIRE(dsp.predecessors[c] == d);
		GRAPH_CHECK_COST(dsp, c, 4);
		GRAPH_CHECK_PRECEDING_EDGE(dsp, c, cd);

		REQUIRE(dsp.predecessors[d] == d);
		GRAPH_CHECK_COST(dsp, d, 0);
		GRAPH_CHECK_PRECEDING_EDGE(dsp, d, edge_const_descriptor_t{});
	}

	template <typename... Args, typename F>
	void doTraversal(F&& dspChecker, Args&&... args) const
	{
		INFO("a = " << a);
		INFO("b = " << b);
		INFO("c = " << c);
		INFO("d = " << d);
		INFO("ab = " << ab);
		INFO("ac = " << ac);
		INFO("cd = " << cd);
		INFO("bd = " << bd);
		auto costFunctor = [&](auto e) { return this->getEdgeCost(e); };
		{
			Dijkstra dsp{g, costFunctor};
			auto t = trace(dsp);
			traverse(g, t, std::forward<Args>(args)...);
			INFO(t.os.str());
			dspChecker(dsp);
		}
		{
			Dijkstra dsp{g, costFunctor};
			traverse(g, dsp, std::forward<Args>(args)...);
			dspChecker(dsp);
		}
	}

	void testDirected() const
	{
		WHEN("Traversing as a directed graph from a")
		{
			doTraversal([&](auto& dsp) { verifyDSPfromA(dsp); }, a);
		}
		WHEN("Traversing as a directed graph from b")
		{
			doTraversal([&](auto& dsp) { verifyDSPfromBDirected(dsp); }, b);
		}
	}

	void testBidi() const
	{
		WHEN("Traversing as a directed graph from a")
		{
			doTraversal([&](auto& dsp) { verifyDSPfromA(dsp); }, a, cxxgraph::DirectedBehavior::Directed);
		}

		WHEN("Traversing as a directed graph from b")
		{
			doTraversal([&](auto& dsp) { verifyDSPfromBDirected(dsp); }, b,
			            cxxgraph::DirectedBehavior::Directed);
		}

		WHEN("Traversing bidirectionally from a")
		{
			doTraversal([&](auto& dsp) { verifyDSPfromA(dsp); }, a,
			            cxxgraph::DirectedBehavior::Bidirectional);
		}

		WHEN("Traversing bidirectionally from b")
		{
			doTraversal([&](auto& dsp) { verifyDSPfromBUndirected(dsp); }, b,
			            cxxgraph::DirectedBehavior::Bidirectional);
		}

		WHEN("Traversing bidirectionally from b, stopping when d is found")
		{
			auto checker = [&](auto& dsp) {
				REQUIRE(dsp.predecessors[a] == b);
				GRAPH_CHECK_COST(dsp, a, 1);
				GRAPH_CHECK_PRECEDING_EDGE(dsp, a, ab);

				REQUIRE(dsp.predecessors[b] == b);
				GRAPH_CHECK_COST(dsp, b, 0);
				GRAPH_CHECK_PRECEDING_EDGE(dsp, b, edge_const_descriptor_t{});

				REQUIRE(dsp.predecessors[c] == a);
				GRAPH_CHECK_COST_OR_EMPTY(dsp, c, 3);

				REQUIRE(dsp.predecessors[d] == b);
				GRAPH_CHECK_COST(dsp, d, 3);
				GRAPH_CHECK_PRECEDING_EDGE(dsp, d, bd);
			};
			doTraversal(checker, b, cxxgraph::DirectedBehavior::Bidirectional,
			            [&](auto& desc) { return desc == d; });
		}

		WHEN("Traversing reverse-directed from d")
		{
			doTraversal([&](auto& dsp) { verifyDSPfromDReverseDirected(dsp); }, d,
			            cxxgraph::DirectedBehavior::ReverseDirected);
		}
	}
	void testUndir() const
	{
		WHEN("Traversing from a")
		{
			doTraversal([&](auto& dsp) { verifyDSPfromA(dsp); }, a);
		}
		WHEN("Traversing from b")
		{
			doTraversal([&](auto& dsp) { verifyDSPfromBUndirected(dsp); }, b);
		}
		WHEN("Traversing from d")
		{
			doTraversal([&](auto& dsp) { verifyDSPfromDReverseDirected(dsp); }, d);
		}
	}
};
} // namespace

TEST_CASE_METHOD(DijkstraFixture<my_config::VecVecStringStringConfig>, "DSP-VecVecStringString") { testDirected(); }

TEST_CASE_METHOD(DijkstraFixture<my_config::BidiVecVecStringStringConfig>, "DSP-BidiVecVecStringString") { testBidi(); }

#ifndef GRAPH_LIGHTER_TESTS
TEST_CASE_METHOD(DijkstraFixture<my_config::UndirVecVecStringStringConfig>, "DSP-UndirVecVecStringString")
{
	testUndir();
}

TEST_CASE_METHOD(DijkstraFixture<my_config::ListVecStringStringConfig>, "DSP-ListVecStringString") { testDirected(); }

TEST_CASE_METHOD(DijkstraFixture<my_config::VecListStringStringConfig>, "DSP-VecListStringString") { testDirected(); }

TEST_CASE_METHOD(DijkstraFixture<my_config::BidiVecListStringStringConfig>, "DSP-BidiVecListStringString")
{
	testBidi();
}
#endif // !GRAPH_LIGHTER_TESTS

TEST_CASE_METHOD(DijkstraFixture<my_config::UndirVecListStringStringConfig>, "DSP-UndirVecListStringString")
{
	testUndir();
}

TEST_CASE_METHOD(DijkstraFixture<my_config::SmListVecStringStringConfig>, "DSP-SmListVecStringString")
{
	testDirected();
}
