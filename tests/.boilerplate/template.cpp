{%- extends 'base.cpp' -%}
{%- macro fn() -%}{{stem}}.cpp{%- endmacro -%}
{%- block brief %}Tests for cxxgraph/{{stem}}.h{% endblock -%}
{%- block content %}
// Internal Includes
#include "cxxgraph/{{stem}}.h"

// Library Includes
#include <catch2/catch.hpp>

// Standard Includes
// - none

TEST_CASE("{{stem}}")
{

}

{% endblock %}
