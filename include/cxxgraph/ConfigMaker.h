// Copyright 2018, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Header
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#pragma once

// Internal Includes
#include "Fwd.h"

// Library Includes
#include <meta/meta.hpp>

// Standard Includes
#include <type_traits>

namespace cxxgraph {
//! @defgroup config Graph Configuration
// @{

//! Types related to graph configuration.
namespace config {
#ifndef GRAPH_DOCS
	namespace detail {
		struct ConfigMakerElementBase
		{
		};
		struct ConfigInheritableBase : ConfigMakerElementBase
		{
		};
		struct EdgePropertiesBase : ConfigMakerElementBase
		{
		};
		struct VertexPropertiesBase : ConfigMakerElementBase
		{
		};
		struct VertexStorageBase : ConfigMakerElementBase
		{
		};
		struct EdgeStorageBase : ConfigMakerElementBase
		{
		};
		struct DirectednessBase : ConfigInheritableBase
		{
		};
		using empty_t = std::tuple<>;
	} // namespace detail
#endif    // !GRAPH_DOCS

	/*! Specifies the per-edge data type for a graph.
	 *
	 * Provide this type, with your per-edge data type as the type parameter, as a type parameter to config_maker_t.
	 *
	 * @see EmptyEdgeProperties if you have no per-edge data to store.
	 */
	template <typename PropertiesType>
	struct EdgeProperties : detail::EdgePropertiesBase
	{
		using type = PropertiesType;
	};

	/*! Indicates that you have no per-edge data to store directly in the graph structure.
	 */
	using EmptyEdgeProperties = EdgeProperties<detail::empty_t>;

	/*! Specifies the per-vertex data type for a graph.
	 *
	 * Provide this type, with your per-vertex data type as the type parameter, as a type parameter to
	 * config_maker_t.
	 *
	 * @see EmptyVertexProperties if you have no per-vertex data to store.
	 */
	template <typename PropertiesType>
	struct VertexProperties : detail::VertexPropertiesBase
	{
		using type = PropertiesType;
	};

	/*! Indicates that you have no per-vertex data to store directly in the graph structure.
	 */
	using EmptyVertexProperties = VertexProperties<detail::empty_t>;

	/*! Specifies the storage policy desired for vertex data in a graph.
	 *
	 * Provide this type, with your chosen storage policy as the type parameter, as a type parameter to
	 * config_maker_t.
	 *
	 * @see storagepolicies
	 */
	template <typename VertexStoragePolicyType>
	struct VertexStorage : detail::VertexStorageBase
	{
		using type = VertexStoragePolicyType;
	};

	/*! Specifies the storage policy desired for edge data in a graph.
	 *
	 * Provide this type, with your chosen storage policy as the type parameter, as a type parameter to
	 * config_maker_t.
	 *
	 * @see storagepolicies
	 */
	template <typename EdgeStoragePolicyType>
	struct EdgeStorage : detail::EdgeStorageBase
	{
		using type = EdgeStoragePolicyType;
	};

	/*! Indicates that you would like your graph to be considered "undirected".
	 *
	 * Provide this type as a type parameter to config_maker_t.
	 *
	 * @see BidirectionalDirected and UnidirectionalDirected which are mutually exclusive with Undirected.
	 */
	struct Undirected : detail::DirectednessBase
	{
		using type = directedness_constant_t<Directedness::Undirected>;
	};

	/*! Indicates that you would like your graph to be considered "directed", and **not** store "in-edges" per
	 * vertex.
	 *
	 * Provide this type as a type parameter to config_maker_t.
	 *
	 * This is the default mode, so you can also omit all mention of directedness in your config_maker_t parameters.
	 *
	 * @see Undirected and BidirectionalDirected which are mutually exclusive with UnidirectionalDirected.
	 */
	struct UnidirectionalDirected : detail::DirectednessBase
	{
		using type = directedness_constant_t<Directedness::UnidirectionalDirected>;
	};

	/*! Indicates that you would like your graph to be considered "directed" and to store each vertex's "in-edges"
	 * as well as its out-edges.
	 *
	 * Provide this type as a type parameter to config_maker_t.
	 *
	 * @see Undirected and UnidirectionalDirected which are mutually exclusive with BidirectionalDirected.
	 */
	struct BidirectionalDirected : detail::DirectednessBase
	{
		using type = directedness_constant_t<Directedness::BidirectionalDirected>;
	};

#ifndef GRAPH_DOCS
	namespace detail {
		namespace m = meta;
		template <typename VPolicy, typename VProperties, typename EPolicy, typename EProperties, typename D>
		struct Config
		{
			using vertex_storage_policy = VPolicy;
			using edge_storage_policy = EPolicy;
			using vertex_properties = VProperties;
			using edge_properties = EProperties;
			using directedness_t = D;
		};

		template <typename BaseType>
		using IsTypeDerivedFrom = m::bind_front<m::quote<std::is_base_of>, BaseType>;

		template <typename T, template <typename> class Wrapper>
		using wrap_if_not_empty_tuple_t =
		    std::conditional_t<std::is_same<T, empty_t>::value, m::list<>, m::list<Wrapper<T>>>;

		template <Directedness d>
		constexpr auto directednessToList()
		{
			if constexpr (d == Directedness::Undirected) {
				return m::list<Undirected>{};
			} else if constexpr (d == Directedness::UnidirectionalDirected) {
				return m::list<>{};
			} else if constexpr (d == Directedness::BidirectionalDirected) {
				return m::list<BidirectionalDirected>{};
			} else { // NOLINT
				static_assert(d == Directedness::Undirected ||
				              d == Directedness::UnidirectionalDirected ||
				              d == Directedness::BidirectionalDirected);
			}
		}

		template <typename T>
		struct GetConfigsTrait
		{
			using type = m::list<T>;
		};

		template <typename VPolicy, typename VProperties, typename EPolicy, typename EProperties, typename D>
		struct GetConfigsTrait<Config<VPolicy, VProperties, EPolicy, EProperties, D>>
		{
			// Re-assemble an equivalent config type list.
			using type = m::concat<
			    m::list<VertexStorage<VPolicy>>, wrap_if_not_empty_tuple_t<VProperties, VertexProperties>,
			    m::list<EdgeStorage<EPolicy>>, wrap_if_not_empty_tuple_t<EProperties, EdgeProperties>,
			    decltype(directednessToList<D::value>())>;
		};

		template <typename... Configs>
		struct GetConfigsTrait<m::list<Configs...>>
		{
			using type = m::list<Configs...>;
		};

		template <typename T>
		using GetConfigs = m::_t<GetConfigsTrait<T>>;

		// For each argument, turn it into a list of configs
		template <typename... Configs>
		using GetConfigLists = m::transform<m::list<Configs...>, m::quote<GetConfigs>>;

		// Concatenate all lists.
		template <typename... Configs>
		using ConcatConfigLists = m::concat<GetConfigs<Configs>...>;

		template <typename List>
		using get_first_trait_t = m::_t<m::front<List>>;

		template <typename List, typename ElseType>
		struct GetFirstTraitElse;

		template <typename ElseType>
		struct GetFirstTraitElse<m::list<>, ElseType> : ElseType
		{
		};
		template <typename ElseType, typename First, typename... Remainder>

		struct GetFirstTraitElse<m::list<First, Remainder...>, ElseType> : First
		{
		};
		template <typename List, typename ElseType>
		using get_first_trait_else_t = m::_t<GetFirstTraitElse<List, ElseType>>;

		template <typename ConfigTypeList, typename BaseType>
		using filter_derived_from_t = m::filter<ConfigTypeList, IsTypeDerivedFrom<BaseType>>;

		// Essentially handles "named parameters" to convert to the more concise (in error messages) but easier
		// to make mistakes with, multiple ordered parameter form.
		template <typename... Configs>
		struct MakeConfigMaker
		{
			using ConfigTypeList = ConcatConfigLists<Configs...>;
			// -- VertexStorage
			using vertex_policy_list_t = filter_derived_from_t<ConfigTypeList, VertexStorageBase>;
			static_assert(m::size<vertex_policy_list_t>::value == 1,
			              "Must include exactly 1 VertexStorage<yourStoragePolicy> in your config list.");
			using vertex_policy_t = get_first_trait_t<vertex_policy_list_t>;

			// -- VertexProperties
			using vertex_property_list_t = filter_derived_from_t<ConfigTypeList, VertexPropertiesBase>;
			static_assert(m::size<vertex_property_list_t>() <= 1,
			              "Must include at most 1 VertexProperties<yourtype> or EmptyVertexProperties in "
			              "your config list.");
			using vertex_properties_t =
			    get_first_trait_else_t<vertex_property_list_t, EmptyVertexProperties>;

			// -- EdgeStorage
			using edge_policy_list_t = filter_derived_from_t<ConfigTypeList, EdgeStorageBase>;
			static_assert(m::size<edge_policy_list_t>() == 1,
			              "Must include exactly 1 EdgeStorage<yourStoragePolicy> in your config list.");
			using edge_policy_t = get_first_trait_t<edge_policy_list_t>;

			// -- EdgeProperties
			using edge_property_list_t = filter_derived_from_t<ConfigTypeList, EdgePropertiesBase>;
			static_assert(m::size<edge_property_list_t>() <= 1,
			              "Must include at most 1 EdgeProperties<yourtype> or EmptyEdgeProperties in your "
			              "config list.");
			using edge_properties_t = get_first_trait_else_t<edge_property_list_t, EmptyEdgeProperties>;

			// -- Directedness
			using directedness_list_t = filter_derived_from_t<ConfigTypeList, DirectednessBase>;
			static_assert(m::size<directedness_list_t>() <= 1,
			              "Must include at most 1 of Undirected, BidirectionalDirected, and "
			              "UnidirectionalDirected in your config list.");
			using directedness_t = get_first_trait_else_t<directedness_list_t, UnidirectionalDirected>;

			// -- Nothing else!
			static_assert(m::size<filter_derived_from_t<ConfigTypeList, ConfigMakerElementBase>>() ==
			                  m::size<ConfigTypeList>(),
			              "All elements of the config list must be one of the established members of "
			              "cxxgraph::config intended for this purpose.");

			using type = Config<vertex_policy_t, vertex_properties_t, edge_policy_t, edge_properties_t,
			                    directedness_t>;
		};
	} // namespace detail

#endif // !GRAPH_DOCS

	/*! Alias template to create a graph configuration struct.
	 *
	 * Pass the following type parameters:
	 *
	 * - exactly one of either EdgeProperties<T> or EmptyEdgeProperties
	 * - exactly one of either VertexProperties<T> or EmptyVertexProperties
	 * - exactly one EdgeStorage<T>
	 * - exactly one VertexStorage<T>
	 * - at most one of Undirected, BidirectionalDirected, or UnidirectionalDirected (the default if none
	 * from this collection are passed)
	 *
	 * Order makes no difference.
	 * A static assert will trigger if you don't satisfy these requirements.
	 *
	 */
	template <typename... Configs>
	using config_maker_t = typename detail::MakeConfigMaker<Configs...>::type;
} // namespace config

//! @}
} // namespace cxxgraph
