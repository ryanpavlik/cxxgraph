// Copyright 2018, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Header
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#pragma once

// Internal Includes
#include "Base.h"

// Library Includes
// - none

// Standard Includes
#include <utility>

namespace cxxgraph {

/*! Return value code from sub-operations of traversal.
 *
 * Indicates if the overall (common) traversal algorithm should continue, skip the "current" element, or terminate
 * operation completely.
 */
enum class ExecutionState
{
	Proceed,   //!< Execution should proceed as usual
	Skip,      //!< Execution should skip processing this element
	EarlyStop, //!< Execution should terminate processing all elements even though it is early
	Finished   //!< Execution has completed
};

//! Convert ExecutionState to a string, primarily for tracing.
constexpr const char* to_string(ExecutionState state) noexcept
{
	switch (state) {
	case ExecutionState::Proceed:
		return "Proceed";
	case ExecutionState::Skip:
		return "Skip";
	case ExecutionState::EarlyStop:
		return "Stop";
	case ExecutionState::Finished:
		return "Finished";
	default:
		return "UNKNOWN";
	}
}

//! true if the execution state indicates that the algorithm should stop executing (Finished or EarlyStop)
constexpr bool shouldStop(ExecutionState state) noexcept
{
	return (state == ExecutionState::EarlyStop) || (state == ExecutionState::Finished);
}
} // namespace cxxgraph

//! Types and functions related to graph traversals/searches
namespace cxxgraph::traversal {
struct ReturnAfterVisitingOne_t;
struct ReturnAfterDrainingQueue_t;
template <typename Derived, typename Graph>
struct TraversalStrategyBase;

//! Given a traversal strategy type, gets the graph type associated with it.
template <typename Strategy>
using graph_from_strategy_t = typename Strategy::graph_t;

//! Given a traversal strategy type, true if the associated graph type is bidirectional-directed.
template <typename Strategy>
constexpr bool is_graph_for_strategy_bidi_t = graph_is_bidirectional_directed_v<graph_from_strategy_t<Strategy>>;

/*! A pair of ExecutionState and a graph's vertex const descriptor type.
 *
 * Returned by traversal steps and by the dequeueAndVisit() method of traversal types.
 */
template <typename Graph>
using state_vertex_descriptor_pair_t = std::pair<ExecutionState, graph_vertex_const_descriptor_t<Graph>>;

/*! The pair of ExecutionState::Finished and the invalid vertex descriptor, for a given graph type.
 *
 * Returned by traversal steps if the queue is fully drained.
 */
template <typename Graph>
constexpr state_vertex_descriptor_pair_t<Graph> traversal_finished_v = {ExecutionState::Finished, {}};

/*! The pair of ExecutionState::EarlyStop and the invalid vertex descriptor, for a given graph type.
 *
 * Returned by traversal steps if the traversal should stop for some reason other than draining the queue.
 */
template <typename Graph>
constexpr state_vertex_descriptor_pair_t<Graph> traversal_early_stop_v = {ExecutionState::EarlyStop, {}};

namespace detail {
	//! A dummy functor that takes any arguments and always returns false.
	struct AlwaysFalse
	{
		//! Function call operator that takes any arguments and returns false unconditionally.
		template <typename... Args>
		bool operator()(Args&&... /*d*/) const noexcept
		{
			return false;
		}
	};
	template <typename Derived>
	struct TraversalReturnPolicyTagBase
	{
		//! CRTP accessor for constant derived type.
		constexpr Derived const& derived() const noexcept { return static_cast<Derived const&>(*this); }

	private:
		// Force CRTP-style inheritance
		constexpr TraversalReturnPolicyTagBase() = default;
		friend Derived;
		// Limit the derived classes to the two intended ones.
		static_assert(std::is_same<Derived, ReturnAfterDrainingQueue_t>() ||
		              std::is_same<Derived, ReturnAfterVisitingOne_t>());
	};

	/*! Implementation function that handles edge ranges associated with a currently-visited vertex.
	 */
	template <typename StrategyDerived, typename Graph, typename Rng, typename F>
	inline auto traverseEdgeRange(TraversalStrategyBase<StrategyDerived, Graph>& strategy,
	                              graph_vertex_const_descriptor_t<Graph> v, Rng&& edgeRange,
	                              F&& earlyStoppingPredicate) -> ExecutionState
	{
		auto& s = strategy.derived();
		for (auto p : edgeRange) {
			auto e = getEdgeDescriptor(p);
			auto otherEnd = getOtherEndOfEdge(p, v);
			auto ret = s.enqueue(e, v, otherEnd, std::forward<F>(earlyStoppingPredicate));
			if (shouldStop(ret)) {
				return ret;
			}
		}
		return ExecutionState::Proceed;
	}

	/*! Implementation function for a generic traversal step or traversal until queue drained.
	 */
	template <typename Graph, typename StrategyDerived, typename F, typename ReturnPolicy>
	inline auto traversalStep(GraphBase<Graph> const& g, TraversalStrategyBase<StrategyDerived, Graph>& strategy,
	                          DirectedBehavior directedness, F&& earlyStoppingPredicate,
	                          detail::TraversalReturnPolicyTagBase<ReturnPolicy> const &
	                          /* dummy */) -> state_vertex_descriptor_pair_t<Graph>
	{
		using graph_t = Graph;
		if constexpr (!graph_is_bidirectional_directed_v<Graph>) {
			if (directedness != DirectedBehavior::Directed) {
				throw std::invalid_argument("The only valid value for directedness in a "
				                            "non-bidirectional-directed graph is "
				                            "DirectedBehavior::Directed");
			}
		}
		if ((directedness & DirectedBehavior::Bidirectional) != directedness) {
			throw std::invalid_argument("The only valid values for directedness are "
			                            "explicitly-named DirectedBehavior types (or "
			                            "bitwise operations between them).");
		}
		if (directedness == DirectedBehavior(0)) {
			throw std::invalid_argument("0 is not a valid value for directedness.");
		}

		using vertex_const_descriptor_t = graph_vertex_const_descriptor_t<graph_t>;

		auto& s = strategy.derived();
		state_vertex_descriptor_pair_t<graph_t> ret = traversal_finished_v<graph_t>;
		auto& state = std::get<ExecutionState>(ret);

		// We use a loop even if we return after visiting a vertex, because we might be told to skip a
		// vertex.
		while (true) {
			ret = s.dequeueAndVisit(std::forward<F>(earlyStoppingPredicate));
			if (ExecutionState::Skip == state) {
				// We already visited this vertex
				continue;
			}
			if (shouldStop(state)) {
				// Return now.
				break;
			}
			vertex_const_descriptor_t v = std::get<vertex_const_descriptor_t>(ret);
			s.beginVerticesConnectedTo(v);
			// Follow all out-edges (or just edges if this is an
			// undirected graph)
			if ((directedness & DirectedBehavior::Directed) != DirectedBehavior(0)) {

				auto state = traverseEdgeRange(s, v, g.derived().vertexOutEdges(v),
				                               std::forward<F>(earlyStoppingPredicate));

				if (shouldStop(state)) {
					// Return now.
					break;
				}
			}
			// Follow all in-edges (if requested)
			if constexpr (graph_is_bidirectional_directed_v<Graph>) {
				if ((directedness & DirectedBehavior::ReverseDirected) != DirectedBehavior(0)) {

					state = traverseEdgeRange(s, v, g.derived().vertexInEdges(v),
					                          std::forward<F>(earlyStoppingPredicate));

					if (shouldStop(state)) {
						// Return now.
						break;
					}
				}
			}
			s.endVerticesConnectedTo(v);

			// If we made it down here, then we've successfully processed one vertex.
			if constexpr (ReturnPolicy::return_after_visit_v) {
				// we are to return after processing a vertex
				return ret;
			}
		}

		// No more vertices to visit.
		state = ExecutionState::EarlyStop;
		return ret;
	}
} // namespace detail

/*! Tag argument to tell the traversal to return after one vertex is successfully visited (and other ends of
 * edges queued, etc.)
 */
constexpr struct ReturnAfterVisitingOne_t : detail::TraversalReturnPolicyTagBase<ReturnAfterVisitingOne_t>
{
	static constexpr bool return_after_visit_v = true;
} return_after_visiting_one;

/*! Tag argument to tell the traversal to continue until the queue is empty.
 *
 * If either enqueue() or dequeueAndVisit() return ExecutionState::EarlyStop before the queue is empty, the traversal
 * will nevertheless return immediately.
 */
constexpr struct ReturnAfterDrainingQueue_t : detail::TraversalReturnPolicyTagBase<ReturnAfterDrainingQueue_t>
{
	static constexpr bool return_after_visit_v = false;
} return_after_draining_queue;

template <typename Derived, typename NestedStrategy>
struct TraversalWrapperBase;

/*! CRTP base for all graph traversal strategies, based on the general graph scan algorithm.
 *
 * See https://idea-instructions.com/graph-scan/ for a clever, nonverbal description of the general process.
 *
 * Note that while method names refer to a queue, this is meant generically: the underlying structure just needs
 * to store "which vertices I should visit", which might be done with a FIFO queue for breadth-first traversal,
 * a LIFO stack for depth-first traversal, etc.
 *
 * Derived classes must provide:
 *
 * ```
 * //! Retrieves the next vertex from the queue, should return traversal_finished_v<graph_t> when no more remain, or
 * //! the pair of ExecutionState::EarlyStop and the visited vertex if it's appropriate to check the predicate and it
 * returns true.
 *
 * template<typename F>
 * auto dequeueAndVisit(F&& earlyStoppingPredicate)
 *     -> std::tuple<vertex_const_descriptor_t, ExecutionState>;
 *
 * //! Called for each vertex on the other end of an edge from the current vertex.
 * //! Returns ExecutionState::EarlyStop if it's appropriate to check the predicate and it returns true, otherwise
 * returns
 * //! ExecutionState::Proceed.
 *
 * template<typename F>
 * auto enqueue(edge_const_descriptor_t e, vertex_const_descriptor_t prev,
 *     vertex_const_descriptor_t v, F&& earlyStoppingPredicate)
 *     -> ExecutionState;
 * ```
 *
 * You may optionally provide the following in a derived class (but not required, since a dummy implementation is
 * provided):
 *
 * ```
 * //! Called before enqueuing vertices connected to v.
 * void beginVerticesConnectedTo(vertex_const_descriptor_t v);
 * //! Called after enqueuing vertices connected to v.
 * void endVerticesConnectedTo(vertex_const_descriptor_t v);
 * ```
 *
 * Notes:
 *
 * - While `enqueue()` is given an edge descriptor, as well as the  vertex to visit and its predecessor
 *   according to that edge, `dequeueAndVisit()` only needs to return the vertex to visit and execution state, so
 *   depending on the specific traversal desired, you do not necessarily need to store all three pieces of data.
 * - The first call to `enqueue()` (and more generally, any calls to `enqueue()` made by enqueueRoot()) will be
 *   trivial, with `e = edge_const_descriptor_t{}`, `predecessor == current`, and a dummy (always false)
 *   `earlyStoppingPredicate`.
 *
 * @ingroup crtpbases
 *
 */
template <typename Derived, typename Graph>
struct TraversalStrategyBase
{
	using graph_t = Graph;
	using vertex_const_descriptor_t = graph_vertex_const_descriptor_t<graph_t>;

	//! CRTP accessor for constant derived type.
	constexpr Derived const& derived() const noexcept { return static_cast<Derived const&>(*this); }

	//! CRTP accessor for derived type.
	constexpr Derived& derived() noexcept { return static_cast<Derived&>(*this); }

	/*! Called before enqueuing vertices connected to v.
	 *
	 * Effectively an event handler - can override in derived class.
	 */
	void beginVerticesConnectedTo(vertex_const_descriptor_t /*v*/) {}

	/*! Called after finished enqueuing vertices connected to v.
	 *
	 * Effectively an event handler - can override in derived class.
	 */
	void endVerticesConnectedTo(vertex_const_descriptor_t /*v*/) {}

	//! Call to enqueue a vertex as its own predecessor (eg for traversal setup).
	auto enqueueRoot(vertex_const_descriptor_t v) -> ExecutionState
	{
		return derived().enqueue(graph_edge_const_descriptor_t<Graph>{}, v, v, detail::AlwaysFalse{});
	}

private:
	constexpr TraversalStrategyBase() = default;
	friend Derived;
	template <typename D, typename NS>
	friend struct TraversalWrapperBase;
};

/*! Base for traversal strategies that mainly wrap other strategies.
 *
 * Provides implementations of all functions that simply delegate to the nested strategy.
 *
 * @ingroup crtpbases
 */
template <typename Derived, typename NestedStrategy>
struct TraversalWrapperBase : TraversalStrategyBase<Derived, graph_from_strategy_t<NestedStrategy>>
{
	using graph_t = graph_from_strategy_t<NestedStrategy>;
	using vertex_const_descriptor_t = graph_vertex_const_descriptor_t<graph_t>;
	using edge_const_descriptor_t = graph_edge_const_descriptor_t<graph_t>;

	//! CRTP accessor for constant derived type.
	constexpr Derived const& derived() const noexcept { return static_cast<Derived const&>(*this); }

	//! CRTP accessor for derived type.
	constexpr Derived& derived() noexcept { return static_cast<Derived&>(*this); }

	constexpr explicit TraversalWrapperBase(NestedStrategy& nestedStrategy) noexcept
	    : nested_(std::ref(nestedStrategy))
	{}

	//! Accessor for contained reference to nested strategy.
	constexpr NestedStrategy& nested() noexcept { return nested_.get(); }

	void beginVerticesConnectedTo(vertex_const_descriptor_t v) { nested().beginVerticesConnectedTo(v); }

	void endVerticesConnectedTo(vertex_const_descriptor_t v) { nested().endVerticesConnectedTo(v); }

	template <typename F>
	auto dequeueAndVisit(F&& earlyStoppingPredicate) -> state_vertex_descriptor_pair_t<graph_t>
	{
		return nested().dequeueAndVisit(std::forward<F>(earlyStoppingPredicate));
	}

	template <typename F>
	auto enqueue(edge_const_descriptor_t e, vertex_const_descriptor_t prev, vertex_const_descriptor_t v,
	             F&& earlyStoppingPredicate) -> ExecutionState
	{
		return nested().enqueue(e, prev, v, std::forward<F>(earlyStoppingPredicate));
	}

private:
	constexpr TraversalWrapperBase() = default;
	friend Derived;
	std::reference_wrapper<NestedStrategy> nested_;
};

/*! Implementation of traversal for undirected or unidirectional-directed graphs.
 *
 * @see Traversal<Strategy, GRAPH_ENABLE_IF(is_graph_for_strategy_bidi_t<Strategy>)>
 */
template <typename Strategy>
struct TraversalImplementationBidi
{
	using graph_t = graph_from_strategy_t<Strategy>;

	//! Constructor
	constexpr TraversalImplementationBidi(graph_t const& g, Strategy& strategy) noexcept
	    : graph_(g), strategy_(strategy)
	{}

	/*! Perform traversal of all vertices reachable from @p source, with a directedness behavior.
	 *
	 * This version of the traversal only works on graphs that are bidirectional-directed.
	 */
	void traverse(graph_vertex_const_descriptor_t<graph_t> source, DirectedBehavior directedness) const
	{
		strategy_.enqueueRoot(source);
		traversalStep(directedness, return_after_draining_queue);
	}

	/*! Perform traversal of the graph starting at @p source with an
	 * early-stopping predicate and a directedness behavior.
	 *
	 * This version of the traversal only works on graphs that are bidirectional-directed.
	 *
	 * Traversal ends when the derived class's dequeueAndVisit() function (which gets the
	 * earlyStoppingPredicate passed to it, which should return true when given a vertex that should be the
	 * last one visited) returns ExecutionState::EarlyStop or ExecutionState::Finished as part of its return pair.
	 * The latter return value indicates that all reachable vertices have been visited.
	 */
	template <typename F>
	void traverse(graph_vertex_const_descriptor_t<graph_t> source, DirectedBehavior directedness,
	              F&& earlyStoppingPredicate) const
	{
		strategy_.enqueueRoot(source);
		traversalStep(directedness, std::forward<F>(earlyStoppingPredicate), return_after_draining_queue);
	}
	/*! Visit a vertex or some vertices, with a directedness behavior.
	 *
	 * This version of the traversal only works on graphs that are bidirectional-directed.
	 *
	 * Note that this will never return ExecutionState::Skip - those are handled internally and automatically since
	 * (by definition) they aren't to be "visited" in the traversal.
	 *
	 * The argument determines whether the traversal continues after successfully visiting one vertex, and
	 * should be either @ref return_after_draining_queue or @ref return_after_visit_one - see their
	 * documentation for details.
	 */
	template <typename ReturnPolicy>
	auto traversalStep(DirectedBehavior directedness,
	                   detail::TraversalReturnPolicyTagBase<ReturnPolicy> const& returnPolicyTag) const
	    -> state_vertex_descriptor_pair_t<graph_t>
	{
		// never stop early: always return false from predicate
		return traversalStep(directedness, detail::AlwaysFalse{}, returnPolicyTag);
	}

	/*! Visit a vertex or some vertices, with an early-stopping predicate and a directedness behavior.
	 *
	 * This version of the traversal only works on graphs that are bidirectional-directed.
	 *
	 * Note that this will never return ExecutionState::Skip - those are handled internally and automatically since
	 * (by definition) they aren't to be "visited" in the traversal.
	 *
	 * The last argument determines whether the traversal continues after successfully visiting one vertex,
	 * and should be either @ref return_after_draining_queue or @ref return_after_visit_one - see their
	 * documentation for details.
	 */
	template <typename F, typename ReturnPolicy>
	auto traversalStep(DirectedBehavior directedness, F&& earlyStoppingPredicate,
	                   detail::TraversalReturnPolicyTagBase<ReturnPolicy> const& returnPolicyTag) const
	    -> state_vertex_descriptor_pair_t<graph_t>
	{
		return detail::traversalStep(graph_, strategy_, directedness, std::forward<F>(earlyStoppingPredicate),
		                             returnPolicyTag);
	}

private:
	graph_t const& graph_;
	Strategy& strategy_;
};

/*! Implementation of traversal for undirected or unidirectional-directed graphs.
 *
 * @see Traversal<Strategy, GRAPH_ENABLE_IF(!is_graph_for_strategy_bidi_t<Strategy>)>
 */
template <typename Strategy>
struct TraversalImplementation
{
	using graph_t = graph_from_strategy_t<Strategy>;

	//! Constructor
	constexpr TraversalImplementation(graph_t const& g, Strategy& strategy) noexcept
	    : graph_(g), strategy_(strategy)
	{}

	/*! Perform traversal of all vertices reachable from @p source.
	 *
	 * This version of the traversal only works on graphs that are not bidirectional-directed.
	 */
	void traverse(graph_vertex_const_descriptor_t<graph_t> source) const
	{
		strategy_.enqueueRoot(source);
		traversalStep(return_after_draining_queue);
	}

	/*! Perform traversal of the graph starting at @p source with an
	 * early-stopping predicate.
	 *
	 * This version of the traversal only works on graphs that are not bidirectional-directed.
	 *
	 * Traversal ends when the derived class's dequeueAndVisit() function (which gets the
	 * earlyStoppingPredicate passed to it, which should return true when given a vertex that should be the
	 * last one visited) returns ExecutionState::EarlyStop or ExecutionState::Finished as part of its return pair.
	 * The latter return value indicates that all reachable vertices have been visited.
	 */
	template <typename F>
	void traverse(graph_vertex_const_descriptor_t<graph_t> source, F&& earlyStoppingPredicate) const
	{
		strategy_.enqueueRoot(source);
		traversalStep(std::forward<F>(earlyStoppingPredicate), return_after_draining_queue);
	}

	/*! Visit a vertex or some vertices.
	 *
	 * This version of the traversal only works on graphs that are not bidirectional-directed.
	 *
	 * The argument determines whether the traversal continues after successfully visiting one vertex, and
	 * should be either @ref return_after_draining_queue or @ref return_after_visit_one - see their
	 * documentation for details.
	 */
	template <typename ReturnPolicy>
	auto traversalStep(detail::TraversalReturnPolicyTagBase<ReturnPolicy> const& returnPolicyTag) const
	    -> state_vertex_descriptor_pair_t<graph_t>
	{
		// never stop early: always return false from predicate
		return traversalStep(detail::AlwaysFalse{}, returnPolicyTag);
	}

	/*! Visit a vertex or some vertices, with an early-stopping predicate.
	 *
	 * This version of the traversal only works on graphs that are not bidirectional-directed.
	 *
	 * The last argument determines whether the traversal continues after successfully visiting one vertex,
	 * and should be either @ref return_after_draining_queue or @ref return_after_visit_one - see their
	 * documentation for details.
	 */
	template <typename F, typename ReturnPolicy>
	auto traversalStep(F&& earlyStoppingPredicate,
	                   detail::TraversalReturnPolicyTagBase<ReturnPolicy> const& returnPolicyTag) const
	    -> state_vertex_descriptor_pair_t<graph_t>
	{
		return detail::traversalStep(graph_, strategy_, DirectedBehavior::Directed,
		                             std::forward<F>(earlyStoppingPredicate), returnPolicyTag);
	}

private:
	graph_t const& graph_;
	Strategy& strategy_;
};

/*! Type that provides a number of `traverse()` and `traversalStep()` overloads to drive the traversal process
 * through a given strategy.
 *
 * Partial specializations mean that the actual interface of this type is provided by one of the following
 * types:
 *
 * @see TraversalImplementation (for undirected or unidirectional-directed graphs)
 * @see TraversalImplementationBidi (for bidirectional-directed graphs).
 */
template <typename Strategy, typename = void>
struct Traversal;

/*! Partial specialization of traversal for bidirectional-directed graphs.
 *
 * @see TraversalImplementationBidi for all implementation.
 */
template <typename Strategy>
struct Traversal<Strategy, GRAPH_ENABLE_IF(is_graph_for_strategy_bidi_t<Strategy>)>
    : TraversalImplementationBidi<Strategy>
{
	using graph_t = graph_from_strategy_t<Strategy>;
	constexpr Traversal(graph_t const& g, Strategy& strategy) noexcept
	    : TraversalImplementationBidi<Strategy>(g, strategy)
	{}
};

/*! Partial specialization of traversal for undirected or unidirectional-directed graphs.
 *
 * @see TraversalImplementation for all implementation.
 */
template <typename Strategy>
struct Traversal<Strategy, GRAPH_ENABLE_IF(!is_graph_for_strategy_bidi_t<Strategy>)> : TraversalImplementation<Strategy>
{
	using graph_t = graph_from_strategy_t<Strategy>;
	constexpr Traversal(graph_t const& g, Strategy& strategy) noexcept
	    : TraversalImplementation<Strategy>(g, strategy)
	{}
};

/*! Perform traversal of all vertices reachable from @p source.
 *
 * This version of the traversal only works on graphs that are not bidirectional-directed.
 *
 * @see TraversalImplementation::traverse()
 */
template <typename Graph, typename StrategyDerived>
inline auto traverse(GraphBase<Graph> const& g, TraversalStrategyBase<StrategyDerived, Graph>& strategy,
                     graph_vertex_const_descriptor_t<Graph> source)
    -> GRAPH_ENABLE_IF(!graph_is_bidirectional_directed_v<Graph>)
{
	Traversal<StrategyDerived> t{g.derived(), strategy.derived()};
	t.traverse(source);
}

/*! Perform traversal of the graph starting at @p source with an early-stopping predicate.
 *
 * This version of the traversal only works on graphs that are not bidirectional-directed.
 *
 * Traversal ends when the derived class's dequeueAndVisit() function (which gets the earlyStoppingPredicate passed to
 * it, which should return true when given a vertex that should be the last one visited) returns
 * ExecutionState::EarlyStop or ExecutionState::Finished as part of its return pair. The latter return value indicates
 * that all reachable vertices have been visited.
 *
 * @see TraversalImplementation<>::traverse()
 */
template <typename Graph, typename StrategyDerived, typename F>
inline auto traverse(GraphBase<Graph> const& g, TraversalStrategyBase<StrategyDerived, Graph>& strategy,
                     graph_vertex_const_descriptor_t<Graph> source, F&& earlyStoppingPredicate)
    -> GRAPH_ENABLE_IF(!graph_is_bidirectional_directed_v<Graph> && !std::is_same<DirectedBehavior, F>::value)
{
	Traversal<StrategyDerived> t{g.derived(), strategy.derived()};
	t.traverse(source, std::forward<F>(earlyStoppingPredicate));
}

/*! Perform traversal of all vertices reachable from @p source with a directedness behavior.
 *
 * This version of the traversal only works on graphs that are bidirectional-directed.
 *
 * @see TraversalImplementationBidi<>::traverse()
 */
template <typename Graph, typename StrategyDerived>
inline auto traverse(GraphBase<Graph> const& g, TraversalStrategyBase<StrategyDerived, Graph>& strategy,
                     graph_vertex_const_descriptor_t<Graph> source, DirectedBehavior directedness)
    -> GRAPH_ENABLE_IF(graph_is_bidirectional_directed_v<Graph>)
{
	Traversal<StrategyDerived> t{g.derived(), strategy.derived()};
	t.traverse(source, directedness);
}

/*! Perform traversal of the graph starting at @p source with an early-stopping predicate and a directedness behavior.
 *
 * This version of the traversal only works on graphs that are bidirectional-directed.
 *
 * Traversal ends when the derived class's dequeueAndVisit() function (which gets the earlyStoppingPredicate passed to
 * it, which should return true when given a vertex that should be the last one visited) returns
 * ExecutionState::EarlyStop or ExecutionState::Finished as part of its return pair. The latter return value indicates
 * that all reachable vertices have been visited.
 *
 * @see TraversalImplementation<>::traverse()
 */
template <typename Graph, typename StrategyDerived, typename F>
inline auto traverse(GraphBase<Graph> const& g, TraversalStrategyBase<StrategyDerived, Graph>& strategy,
                     graph_vertex_const_descriptor_t<Graph> source, DirectedBehavior directedness,
                     F&& earlyStoppingPredicate) -> GRAPH_ENABLE_IF(graph_is_bidirectional_directed_v<Graph>)
{
	Traversal<StrategyDerived> t{g.derived(), strategy.derived()};
	t.traverse(source, directedness, std::forward<F>(earlyStoppingPredicate));
}
} // namespace cxxgraph::traversal
