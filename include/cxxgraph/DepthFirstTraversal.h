// Copyright 2018, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Header
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#pragma once

// Internal Includes
#include "Traversal.h"

// Library Includes
// - none

// Standard Includes
#include <utility>
#include <vector>

namespace cxxgraph::traversal {

/*! Traversal strategy to perform a depth-first search or traversal.
 */
template <typename Graph>
struct DepthFirstTraversal : TraversalStrategyBase<DepthFirstTraversal<Graph>, Graph>
{
	using graph_t = Graph;
	using vertex_const_descriptor_t = graph_vertex_const_descriptor_t<Graph>;
	using predecessor_map_t = graph_descriptor_map_t<Graph, vertex_tag, vertex_const_descriptor_t>;
	using edge_const_descriptor_t = graph_edge_const_descriptor_t<Graph>;

	//! Constructor
	explicit DepthFirstTraversal(Graph const& graph) : predecessors(graph) {}

	template <typename F>
	auto dequeueAndVisit(F&& earlyStoppingPredicate) -> state_vertex_descriptor_pair_t<graph_t>
	{
		if (queue.empty()) {
			return traversal_finished_v<graph_t>;
		}
		vertex_const_descriptor_t prev, v;
		std::tie(prev, v) = queue.back();
		queue.pop_back();
		if (isVisited(v)) {
			return {ExecutionState::Skip, v};
		}
		predecessors[v] = prev;
		if (std::forward<F>(earlyStoppingPredicate)(v)) {
			return {ExecutionState::EarlyStop, v};
		}
		return {ExecutionState::Proceed, v};
	}

	template <typename F>
	auto enqueue(edge_const_descriptor_t /*e*/, vertex_const_descriptor_t prev, vertex_const_descriptor_t v, F &&
	             /*earlyStoppingPredicate*/) -> ExecutionState
	{
		if (!isVisited(v)) {
			// Only queue if we haven't already visited this vertex.
			queue.emplace_back(prev, v);
			return ExecutionState::Proceed;
		}
		// We already visited this one.
		return ExecutionState::Skip;
	}

	/*! True if we have visited v.
	 */
	bool isVisited(vertex_const_descriptor_t v) const { return predecessors[v] != vertex_const_descriptor_t{}; }

	predecessor_map_t predecessors;
	std::vector<std::pair<vertex_const_descriptor_t, vertex_const_descriptor_t>> queue;
};
} // namespace cxxgraph::traversal
