// Copyright 2018, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Header
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#pragma once

// Internal Includes
// - none

// Library Includes
// - none

// Standard Includes
#include <type_traits>

namespace cxxgraph {

//! @defgroup entitytag Entity Tag Types
//! @{

//! Tag type used as type parameter to indicate "edge".
struct edge_tag;

//! Tag type used as type parameter to indicate "vertex".
struct vertex_tag;
//! @}

//! Directedness of a graph
enum class Directedness
{
	Undirected,             //!< Source and target of edges are interchangeable
	UnidirectionalDirected, //!< Edges are directed, in-edges are not stored
	BidirectionalDirected,  //!< Edges are directed, in-edges are stored
};

template <Directedness D>
using directedness_constant_t = std::integral_constant<Directedness, D>;
} // namespace cxxgraph
