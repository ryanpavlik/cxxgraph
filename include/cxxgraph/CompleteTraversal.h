// Copyright 2018, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Header
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#pragma once

// Internal Includes
#include "Traversal.h"

// Library Includes
// - none

// Standard Includes
// - none

namespace cxxgraph::traversal {

/*! A traversal strategy that wraps a real traversal strategy, and ensures that all vertices are visited.
 *
 * Enqueues an unvisited vertex when the wrapped strategy reports that it is out of vertices in the queue.
 *
 * Use by `auto myCompleteStrategy = completeTraversal(graph, mainStrategy);` and then passing `myCompleteStrategy`
 * instead of `mainStrategy` to traverse()
 */
template <typename NestedStrategy>
struct CompleteTraversalStrategy : TraversalWrapperBase<CompleteTraversalStrategy<NestedStrategy>, NestedStrategy>
{
	using base_t = TraversalWrapperBase<CompleteTraversalStrategy<NestedStrategy>, NestedStrategy>;
	using base_t::nested;

	using graph_t = graph_from_strategy_t<NestedStrategy>;
	using vertex_const_descriptor_t = graph_vertex_const_descriptor_t<graph_t>;
	using visited_map_t = graph_descriptor_map_t<graph_t, vertex_tag, bool>;
	std::reference_wrapper<graph_t const> graph;
	visited_map_t visited;

	constexpr explicit CompleteTraversalStrategy(graph_t const& graph_, NestedStrategy& nested_) noexcept
	    : base_t(nested_), graph(std::cref(graph_)), visited(graph_)
	{}

	auto enqueueUnvisitedVertex() -> ExecutionState
	{
		for (auto v : graph.get().vertexDescriptors()) {
			if (!visited[v]) {
				return this->enqueueRoot(v);
			}
		}
		return ExecutionState::Proceed;
	}

	template <typename F>
	auto dequeueAndVisit(F&& earlyStoppingPredicate) -> state_vertex_descriptor_pair_t<graph_t>
	{
		vertex_const_descriptor_t v;
		cxxgraph::ExecutionState state;
		std::tie(state, v) = nested().dequeueAndVisit(std::forward<F>(earlyStoppingPredicate));
		if (state == ExecutionState::Finished) {
			// Try to enqueue an unvisited vertex then dequeue it.
			auto s = enqueueUnvisitedVertex();
			if (shouldStop(s)) {
				return {s, v};
			}
			std::tie(state, v) = nested().dequeueAndVisit(std::forward<F>(earlyStoppingPredicate));
		}

		if (state == ExecutionState::Proceed) {
			// Record that we're visiting this vertex.
			visited[v] = true;
		}
		return {state, v};
	}
};

//! Helper function to wrap a traversal strategy to ensure all vertices are visited, deducing all types.
template <typename NestedStrategy, typename Graph>
constexpr auto completeTraversal(meta::id_t<Graph> const& g, TraversalStrategyBase<NestedStrategy, Graph>& strategy)
    -> CompleteTraversalStrategy<NestedStrategy>
{
	return CompleteTraversalStrategy<NestedStrategy>{g, strategy.derived()};
}

} // namespace cxxgraph::traversal
