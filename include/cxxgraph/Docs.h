// Copyright 2018, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/* File used only by Doxygen for documentation directives that don't neatly fit into an existing source file. */

#pragma once

//! The namespace used for all functionality in this library.
namespace cxxgraph {

/*! @defgroup crtpbases CRTP Bases
 *
 * @brief Base type templates for use with the "Curiously Recurring Template Pattern", providing interfaces, common
 * functionality, and static polymorphism.
 *
 * - All are intended for use by a base class that should pass its own type as a type parameter, which is typically
 *   named `Derived`. (Some take more than one type parameter.)
 * - All CRTP bases shall have members named `derived()` that return a reference to the (possibly const-qualified)
 *   derived type.
 * - CRTP bases typically, though not necessarily, also have a defaulted constructor taking no arguments declared as
 *   private, as well as a friend declaration for `Derived`. This is a technique, discussed at
 *   https://www.fluentcpp.com/2017/05/12/curiously-recurring-template-pattern/, for ensuring that classes derive from
 *   the correct specialization of a CRTP base (i.e. so that an erroneous construction like
 *   `class A : public CRTPBase<B> {};` does not compile.)
 *
 * For more information on the CRTP, see:
 *
 * - The wikipedia entry: https://en.wikipedia.org/wiki/Curiously_recurring_template_pattern
 * - The docs of the Eigen linear-algebra library which uses the pattern to provide a hierarchy of interfaces, and which
 *   does a good job of explaining it in a practical way:
 *   http://eigen.tuxfamily.org/dox/TopicFunctionTakingEigenTypes.html
 */

/*! @defgroup extendgraph If You're Adding a New Graph Type
 *
 * @brief Functionality mainly used in the process of implementing a new graph type template, rather than (for instance)
 * using an existing graph type template with your own configuration.
 */

/*! @defgroup storageimpl Details of Storage Policy Implementation
 *
 * @brief Details primarily interesting to those adding or adapting element storage policies.
 *
 * @sa traits for type alias templates and variable templates.
 */

/*! @defgroup traits Type Traits
 *
 * @brief Type alias templates and variable templates for computing types or compile-time constants associated with
 * various types.
 */

/*! @defgroup impl Implementation Details
 *
 * @brief Implementation details worth having generated documentation for, but not of interest to someone not hacking on
 * the core of the library.
 */

/*! Element storage policies and closely-related types and traits.
 */
namespace policy {

} // namespace policy
} // namespace cxxgraph
