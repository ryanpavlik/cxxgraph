# Copyright (c) 2018 Collabora, Ltd.
#
# SPDX-License-Identifier: BSL-1.0

add_subdirectory(cxxgraph)
