# Copyright (c) 2018 Collabora, Ltd.
#
# SPDX-License-Identifier: BSL-1.0

#if("${CMAKE_CXX_COMPILER_ID}" MATCHES "Clang")
#    set(IS_CLANG TRUE)
#else()
#set(IS_CLANG FALSE)
#endif()
find_program(LLVM_COV_COMMAND
    NAMES
    llvm-cov
    llvm-cov-8
    llvm-cov-7
    llvm-cov-6.0)
find_program(LLVM_PROFDATA_COMMAND
    NAMES
    llvm-profdata
    llvm-profdata-8
    llvm-profdata-7
    llvm-profdata-6.0)

set(COVERAGE_CAPABLE OFF)
if(("${CMAKE_CXX_COMPILER_ID}" MATCHES "Clang") AND ("${CMAKE_BUILD_TYPE}" STREQUAL "Debug") AND LLVM_COV_COMMAND AND LLVM_PROFDATA_COMMAND)
    set(COVERAGE_CAPABLE ON)
endif()
cmake_dependent_option(BUILD_COVERAGE "Enable LLVM-based code coverage reporting?" ON
    COVERAGE_CAPABLE OFF)

if(BUILD_COVERAGE)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fprofile-instr-generate -fcoverage-mapping")

    find_program(CXXFILT_COMMAND
        NAMES c++filt)
    function(graph_add_coverage COVTARGET TARGET)
        set(RAW_FILE "${CMAKE_CURRENT_BINARY_DIR}/coverage-${TARGET}.profraw")
        set(DATA_FILE "${CMAKE_CURRENT_BINARY_DIR}/coverage-${TARGET}.profdata")
        set(REPORT_DIR "${CMAKE_CURRENT_BINARY_DIR}/${COVTARGET}")
        set(CXXFILT_ARGS)
        if(CXXFILT_COMMAND)
            set(CXXFILT_ARGS -Xdemangler ${CXXFILT_COMMAND} -Xdemangler -n)
        endif()
        add_custom_command(OUTPUT "${RAW_FILE}"
            COMMAND
            LLVM_PROFILE_FILE=${RAW_FILE} $<TARGET_FILE:${TARGET}>
            DEPENDS $<TARGET_FILE:${TARGET}>
            COMMENT "Running ${TARGET} with coverage enabled"
            USES_TERMINAL
            VERBATIM)
        add_custom_command(OUTPUT "${DATA_FILE}"
            COMMAND
            ${LLVM_PROFDATA_COMMAND} merge -sparse ${RAW_FILE} -o ${DATA_FILE}
            DEPENDS ${RAW_FILE}
            COMMENT "Merging coverage data for ${TARGET}"
            VERBATIM)
        get_target_property(SOURCES ${PROJECT_NAME} INTERFACE_SOURCES)
        set(ABSOLUTE_SOURCES)
        foreach(SOURCE ${SOURCES})
            if(IS_ABSOLUTE ${SOURCE})
                list(APPEND ABSOLUTE_SOURCES ${SOURCE})
            else()
                list(APPEND ABSOLUTE_SOURCES ${PROJECT_SOURCE_DIR}/include/cxxgraph/${SOURCE})
            endif()
        endforeach()
        add_custom_target(${COVTARGET}
            ${LLVM_COV_COMMAND}
            show
            $<TARGET_FILE:${TARGET}>
            -instr-profile=${DATA_FILE}
            -show-line-counts-or-regions
            #-show-instantiations=false
            -output-dir=${REPORT_DIR}
            -format=html
            ${CXXFILT_ARGS}
            ${ABSOLUTE_SOURCES}
            DEPENDS "${DATA_FILE}"
            COMMENT "Generating coverage report for ${TARGET} - view ${REPORT_DIR}/index.html to see results"
            VERBATIM)
    endfunction()
else()
    # Dummy impl to avoid needing lots of conditionals.
    function(graph_add_coverage)
    endfunction()
endif()
